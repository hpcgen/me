dnl workaround for old automake on darwin

AC_DEFUN([AM_CONFIG_HEADERS], [AC_CONFIG_HEADERS($@)])

dnl set flags according to build environment

AC_DEFUN([SHERPA_SETUP_BUILDSYSTEM],
[
  case "$build_os:$build_cpu:$build_vendor" in
    *darwin*:*:*)
      echo "checking for architecture... Darwin MacOS"
      if test "x$LDFLAGS" = "x"; then
        AM_LDFLAGS="-dynamic -flat_namespace"
      fi
      SEDCOMMAND="sed -i.bak -E"
      AC_DEFINE([ARCH_DARWIN], "1", [Architecture identified as Darwin MacOS])
      AC_DEFINE([LIB_SUFFIX], ".dylib", [library suffix set to .dylib]) 
      AC_DEFINE([LD_PATH_NAME], "DYLD_LIBRARY_PATH", [ld path name set to DYLD_LIBRARY_PATH]) ;;
    *linux*:*:*)
      echo "checking for architecture...  Linux"
      if test "x$LDFLAGS" = "x"; then
        AM_LDFLAGS="-rdynamic -Wl,--no-as-needed"
      fi
      SEDCOMMAND="sed -i -r"
      AC_DEFINE([ARCH_LINUX], "1", [Architecture identified as Linux])
      AC_DEFINE([LIB_SUFFIX], ".so", [library suffix set to .so]) 
      AC_DEFINE([LD_PATH_NAME], "LD_LIBRARY_PATH", [ld path name set to LD_LIBRARY_PATH]) ;;
    *)
      echo "checking for architecture...  unknown"
      echo "hosts system type $build not yet supported, assuming unix behaviour."
      echo "possible failure due to unknown compiler/linker characteristics."
      echo "please inform us about build results at sherpa@projects.hepforge.org"
      echo "(will continue in 10 seconds)"
      sleep 10
      if test "x$LDFLAGS" = "x"; then
        AM_LDFLAGS="-rdynamic -Wl,--no-as-needed"
      fi
      SEDCOMMAND="sed -i -r"
      AC_DEFINE([ARCH_UNIX], "1", [Architecture identified as Unix])
      AC_DEFINE([LIB_SUFFIX], ".so", [library suffix set to .so]) 
      AC_DEFINE([LD_PATH_NAME], "LD_LIBRARY_PATH", [ld path name set to LD_LIBRARY_PATH]) ;;
  esac
  AC_SUBST(AM_LDFLAGS)
  if which md5sum > /dev/null; then MD5COMMAND="md5sum | cut -d' ' -f1";
  elif which openssl > /dev/null; then MD5COMMAND="openssl md5 | cut -d' ' -f2";
  else MD5COMMAND="echo 'X'"; fi
  AC_SUBST(MD5COMMAND)
  AC_SUBST(SEDCOMMAND)

  if test "x$CXXFLAGS" == "x"; then CXXFLAGS=""; fi
])


AC_DEFUN([AS_AC_EXPAND],
[
  full_var="[$2]"
  numbers="1 2 3 4"
  for i in $numbers; do
    full_var="`eval echo $full_var`";
  done
  AC_SUBST([$1], "$full_var")
])


dnl setup all variables for substitution in Makefile.am's and some additional DEFINEs
dnl
dnl Additionally some variables are defined automatically:
dnl @bindir@  executables' directory
dnl @datadir@  specified data directory e. g. /usr/local/share
dnl @includedir@  directory where header files are being installed
dnl @libdir@  directory where libraries are being installed
dnl @prefix@  the common installation prefix, e. g. /usr/local
dnl @top_builddir@  relative path to the top-level of build tree


AC_DEFUN([SHERPA_SETUP_VARIABLES],
[
  if test "x$VERSIONING" != "x"; then
    echo "x$VERSIONING";
    pkgdatadir="\${datadir}/\${PACKAGE_TARNAME}-\${VERSIONING}";
    AC_SUBST(pkgdatadir)
    pkglibdir="\${libdir}/\${PACKAGE_TARNAME}-\${VERSIONING}";
    AC_SUBST(pkglibdir)
    pkgincludedir="\${includedir}/\${PACKAGE_TARNAME}-\${VERSIONING}";
    AC_SUBST(pkgincludedir)
  else
    pkgdatadir="\${datadir}/\${PACKAGE_TARNAME}";
    AC_SUBST(pkgdatadir)
    pkglibdir="\${libdir}/\${PACKAGE_TARNAME}";
    AC_SUBST(pkglibdir)
    pkgincludedir="\${includedir}/\${PACKAGE_TARNAME}";
    AC_SUBST(pkgincludedir)
  fi;
  
  if test "x$docdir" = "x"; then
    docdir="\${datadir}/doc/\${PACKAGE_TARNAME}";
    AC_SUBST(docdir)
  fi;

  if test "x$htmldir" = "x"; then
    htmldir="\${docdir}";
    AC_SUBST(htmldir)
  fi;

  AMEGICDIR="\${top_srcdir}/AMEGIC++"
  AMEGICBUILDDIR="\${top_builddir}/AMEGIC++"
  AMEGICLIBS="\${AMEGICBUILDDIR}/Main/libAmegic.la \
	\${AMEGICBUILDDIR}/DipoleSubtraction/libDipoleSubtraction.la \
	\${AMEGICBUILDDIR}/Amplitude/libAmplitude.la \
	\${AMEGICBUILDDIR}/Phasespace/libAmegicPSGen.la \
	\${AMEGICBUILDDIR}/String/libString.la \
	\${AMEGICBUILDDIR}/Amplitude/Zfunctions/libZfunctions.la"
  AC_SUBST(AMEGICDIR)
  AC_SUBST(AMEGICBUILDDIR)
  AC_SUBST(AMEGICLIBS)

  EXTAMPDIR="\${top_srcdir}/EXTAMP"
  EXTAMPBUILDDIR="\${top_builddir}/EXTAMP"
  EXTAMPLIBS="\${EXTAMPBUILDDIR}/libExtAmp.la"
  AC_SUBST(EXTAMPDIR)
  AC_SUBST(EXTAMPBUILDDIR)
  AC_SUBST(EXTAMPLIBS)

  ATOOLSDIR="\${top_srcdir}/ATOOLS"
  ATOOLSBUILDDIR="\${top_builddir}/ATOOLS"
  ATOOLSLIBS="\${ATOOLSBUILDDIR}/Phys/libToolsPhys.la \
	\${ATOOLSBUILDDIR}/Math/libToolsMath.la \
	\${ATOOLSBUILDDIR}/Org/libToolsOrg.la"
  AC_SUBST(ATOOLSDIR)
  AC_SUBST(ATOOLSBUILDDIR)
  AC_SUBST(ATOOLSLIBS)
  
  BEAMDIR="\${top_srcdir}/BEAM"
  BEAMBUILDDIR="\${top_builddir}/BEAM"
  BEAMLIBS="\${BEAMBUILDDIR}/Main/libBeam.la"
  AC_SUBST(BEAMDIR)
  AC_SUBST(BEAMBUILDDIR)
  AC_SUBST(BEAMLIBS)

  METOOLSDIR="\${top_srcdir}/METOOLS"
  METOOLSBUILDDIR="\${top_builddir}/METOOLS"
  METOOLSLIBS="\${METOOLSBUILDDIR}/Explicit/libMEToolsExplicit.la \
	\${METOOLSBUILDDIR}/Currents/libMEToolsCurrents.la \
	\${METOOLSBUILDDIR}/Vertices/libMEToolsVertices.la \
	\${METOOLSBUILDDIR}/Colors/libMEToolsColors.la \
	\${METOOLSBUILDDIR}/SpinCorrelations/libMEToolsSpinCorrelations.la \
	\${METOOLSBUILDDIR}/Loops/libMEToolsLoops.la \
	\${METOOLSBUILDDIR}/Main/libMEToolsMain.la"
  AC_SUBST(METOOLSDIR)
  AC_SUBST(METOOLSBUILDDIR)
  AC_SUBST(METOOLSLIBS)
  
  EXTRAXSDIR="\${top_srcdir}/EXTRA_XS"
  EXTRAXSBUILDDIR="\${top_builddir}/EXTRA_XS"
  EXTRAXSLIBS="\${EXTRAXSBUILDDIR}/Main/libExtraXS.la \
	\${EXTRAXSBUILDDIR}/Two2Two/libExtraXS2_2.la \
	\${EXTRAXSBUILDDIR}/One2Two/libExtraXS1_2.la \
	\${EXTRAXSBUILDDIR}/One2Three/libExtraXS1_3.la \
	\${EXTRAXSBUILDDIR}/NLO/libExtraXSNLO.la"
  AC_SUBST(EXTRAXSDIR)
  AC_SUBST(EXTRAXSBUILDDIR)
  AC_SUBST(EXTRAXSLIBS)
  
  MCATNLODIR="\${top_srcdir}/MCATNLO"
  MCATNLOBUILDDIR="\${top_builddir}/MCATNLO"
  MCATNLOLIBS="\${MCATNLOBUILDDIR}/Main/libMCatNLOMain.la \
	\${MCATNLOBUILDDIR}/Calculators/libMCatNLOCalculators.la \
	\${MCATNLOBUILDDIR}/Showers/libMCatNLOShowers.la \
	\${MCATNLOBUILDDIR}/Tools/libMCatNLOTools.la"
  AC_SUBST(MCATNLODIR)
  AC_SUBST(MCATNLOBUILDDIR)
  AC_SUBST(MCATNLOLIBS)
  
  DIMDIR="\${top_srcdir}/DIM"
  DIMBUILDDIR="\${top_builddir}/DIM"
  DIMLIBS="\${DIMBUILDDIR}/Tools/libDIMTools.la \
	\${DIMBUILDDIR}/Shower/libDIMShower.la \
	\${DIMBUILDDIR}/Gauge/libDIMGauge.la \
	\${DIMBUILDDIR}/Lorentz/libDIMLorentz.la \
	\${DIMBUILDDIR}/Main/libDIMMain.la"
  AC_SUBST(DIMDIR)
  AC_SUBST(DIMBUILDDIR)
  AC_SUBST(DIMLIBS)
  
  COMIXDIR="\${top_srcdir}/COMIX"
  COMIXBUILDDIR="\${top_builddir}/COMIX"
  COMIXLIBS="\${COMIXBUILDDIR}/Amplitude/libComixAmplitude.la \
	\${COMIXBUILDDIR}/Phasespace/libComixPhasespace.la \
	\${COMIXBUILDDIR}/Main/libComix.la"
  AC_SUBST(COMIXDIR)
  AC_SUBST(COMIXBUILDDIR)
  AC_SUBST(COMIXLIBS)
  
  MODELDIR="\${top_srcdir}/MODEL"
  MODELBUILDDIR="\${top_builddir}/MODEL"
  MODELMAINLIB="\${MODELBUILDDIR}/Main/libModelMain.la"
  MODELLIBS="\${MODELMAINLIB} \
	\${MODELBUILDDIR}/UFO/libModelUFO.la"
  AC_SUBST(MODELDIR)
  AC_SUBST(MODELBUILDDIR)
  AC_SUBST(MODELMAINLIB)
  AC_SUBST(MODELLIBS)
  
  PDFDIR="\${top_srcdir}/PDF"
  PDFBUILDDIR="\${top_builddir}/PDF"
  PDFINCS="-I\${PDFDIR}/Main"
  PDFLIBS="\${PDFBUILDDIR}/Main/libPDF.la"
  AC_SUBST(PDFDIR)
  AC_SUBST(PDFBUILDDIR)
  AC_SUBST(PDFLIBS)
  
  PHASICDIR="\${top_srcdir}/PHASIC++"
  PHASICBUILDDIR="\${top_builddir}/PHASIC++"
  PHASICLIBS="\${PHASICBUILDDIR}/Main/libPhasicMain.la \
	\${PHASICBUILDDIR}/Channels/libPhasicChannels.la \
	\${PHASICBUILDDIR}/Process/libPhasicProcess.la \
	\${PHASICBUILDDIR}/Selectors/libPhasicSelectors.la \
	\${PHASICBUILDDIR}/Scales/libPhasicScales.la \
	\${PHASICBUILDDIR}/Enhance/libPhasicEnhance.la \
	\${PHASICBUILDDIR}/Decays/libPhasicDecays.la"
  AC_SUBST(PHASICDIR)
  AC_SUBST(PHASICBUILDDIR)
  AC_SUBST(PHASICLIBS)
  
  SHERPADIR="\${top_srcdir}/SHERPA"
  SHERPABUILDDIR="\${top_builddir}/SHERPA"
  SHERPALIBS="\${SHERPABUILDDIR}/Initialization/libSherpaInitialization.la \
	\${SHERPABUILDDIR}/Single_Events/libSherpaSingleEvents.la \
	\${SHERPABUILDDIR}/PerturbativePhysics/libSherpaPerturbativePhysics.la \
	\${SHERPABUILDDIR}/Tools/libSherpaTools.la"
  AC_SUBST(SHERPADIR)
  AC_SUBST(SHERPABUILDDIR)
  AC_SUBST(SHERPALIBS)

  if test "x$prefix" = "xNONE"; then
    prefix=$ac_default_prefix
  fi
  if test "x$exec_prefix" = "xNONE"; then
    exec_prefix=$prefix
  fi

  AS_AC_EXPAND(LIBDIR, ${pkglibdir})
  AS_AC_EXPAND(PYLIBDIR, ${pythondir})
  AS_AC_EXPAND(INCLUDEDIR, ${pkgincludedir})
  AS_AC_EXPAND(BINDIR, ${bindir})
  AS_AC_EXPAND(DATADIR, ${pkgdatadir})
  AS_AC_EXPAND(SHERPAPREFIX, ${prefix})
  AS_AC_EXPAND(PYTHONLIBS, ${pythondir})

  AC_DEFINE_UNQUOTED([SHERPA_VERSION], ["`echo AC_PACKAGE_VERSION | cut -d. -f1`"], [Sherpa version])
  AC_DEFINE_UNQUOTED([SHERPA_SUBVERSION], ["`echo AC_PACKAGE_VERSION | cut -d. -f2,3`"], [Sherpa subversion])
  AC_DEFINE_UNQUOTED([SHERPA_PREFIX], "$SHERPAPREFIX", [Sherpa installation prefix])
  AC_DEFINE_UNQUOTED([SHERPA_INCLUDE_PATH], "$INCLUDEDIR", [Sherpa include directory])
  AC_DEFINE_UNQUOTED([SHERPA_LIBRARY_PATH], "$LIBDIR", [Sherpa library directory])
  AC_DEFINE_UNQUOTED([SHERPA_SHARE_PATH], "$DATADIR", [Sherpa data directory])
  AC_DEFINE_UNQUOTED([PYTHON_LIBS], "$PYTHONLIBS", [Sherpa python library directory])
  AC_DEFINE([USING__COLOUR], "1", [Using colour])

  AM_CPPFLAGS="-I\$(top_srcdir)"
  AC_SUBST(AM_CPPFLAGS)

  AM_CXXFLAGS="-g -O2"
  AC_LANG_PUSH([C++])
  AX_CHECK_COMPILE_FLAG(
    -fcx-fortran-rules,
    [AM_CXXFLAGS="${AM_CXXFLAGS} -fcx-fortran-rules"])
  AC_LANG_POP([C++])
  AC_SUBST(AM_CXXFLAGS)

  localincdir="\$(pkgincludedir)/\$(subdir)"
  AC_SUBST(localincdir)
])



dnl Conditional compiling and linking

AC_DEFUN([SHERPA_SETUP_CONFIGURE_OPTIONS],
[
  AC_ARG_ENABLE(
    versioning,
    AC_HELP_STRING([--enable-versioning], [Add version tag to executables and library/header directories, such that multiple Sherpa versions can live in the same prefix.]),
    [ AC_MSG_CHECKING(whether to enable versioning)
      case "${enableval}" in
        no)  AC_MSG_RESULT(no);
             VERSIONING="";;
        yes) AC_MSG_RESULT(yes);
             VERSIONING="AC_PACKAGE_VERSION";;
        *)   if test "x${enableval}" != "x"; then
               AC_MSG_RESULT(yes);
               VERSIONING="${enableval}"
             fi
      esac ],
    [ AC_MSG_CHECKING(whether to enable versioning);
      AC_MSG_RESULT(no);
      VERSIONING=""; ] 
  )
  AC_SUBST(VERSIONING)

  AC_ARG_ENABLE(
    multithread,
    AC_HELP_STRING([--enable-multithread], [Enable multithreading]),
    [ AC_MSG_CHECKING(for multithreading)
      case "${enableval}" in
        no)  AC_MSG_RESULT(no); multithread=false ;;
        yes) AC_MSG_RESULT(yes); multithread=true ;;
      esac ],
    [ AC_MSG_CHECKING(for multithreading); AC_MSG_RESULT(no); multithread=false ] 
  )
  if test "$multithread" = "true" ; then
    AC_DEFINE([USING__Threading], "1", [using multithreading])
    CONDITIONAL_THREADLIBS="-lpthread"
  fi
  AC_SUBST(CONDITIONAL_THREADLIBS)
  AM_CONDITIONAL(USING__Threading, test "$multithread" = "true" )
  
  AC_ARG_ENABLE(
    apes,
    AC_HELP_STRING([--enable-apes=/path/to/apes], [Enable Apes]),
    [ AC_MSG_CHECKING(for Apes installation directory);
      case "${enableval}" in
        no)  AC_MSG_RESULT(Apes not enabled); apes=false ;;
        yes)  if test -x "$APESDIR/bin/apes-config"; then
                CONDITIONAL_APESDIR="$APESDIR"
                CONDITIONAL_APESINCS="-I$($CONDITIONAL_APESDIR/bin/apes-config --includedir)";
                CONDITIONAL_APESLIBS="-L$($CONDITIONAL_APESDIR/bin/apes-config --libdir) -Wl,-rpath -Wl,$($CONDITIONAL_APESDIR/bin/apes-config --libdir) -lapes -lIntegrator -lChannel -lTools -lfmt -lspdlog -lyaml-cpp"
              else
                AC_MSG_ERROR(\$APESDIR is not a valid path.);
              fi;
              AC_MSG_RESULT([${CONDITIONAL_APESDIR}]); apes=true;;
        *)    if test -x "${enableval}/bin/apes-config"; then
                CONDITIONAL_APESDIR="${enableval}"
                CONDITIONAL_APESINCS="-I$($CONDITIONAL_APESDIR/bin/apes-config --includedir)";
                CONDITIONAL_APESLIBS="-L$($CONDITIONAL_APESDIR/bin/apes-config --libdir) -Wl,-rpath -Wl,$($CONDITIONAL_APESDIR/bin/apes-config --libdir) -lapes -lIntegrator -lChannel -lTools -lfmt -lspdlog -lyaml-cpp"
              else
                AC_MSG_ERROR(${enableval} is not a valid path.);
              fi;
              AC_MSG_RESULT([${CONDITIONAL_APESDIR}]); apes=true;;
      esac
      ],
    [ apes=false ]
  )
  if test "$apes" = "true" ; then
    AC_DEFINE_UNQUOTED([APES_PATH], "$CONDITIONAL_APESDIR", [Apes directory])
    AC_DEFINE([USING__APES], "1", [Using APES])
  fi
  AC_SUBST(CONDITIONAL_APESDIR)
  AC_SUBST(CONDITIONAL_APESINCS)
  AC_SUBST(CONDITIONAL_APESLIBS)
  AM_CONDITIONAL(APES_SUPPORT, test "$apes" = "true")

  AC_ARG_ENABLE(
    blackhat,
    AC_HELP_STRING([--enable-blackhat=/path/to/blackhat], [Enable BLACKHAT.]),
    [ AC_MSG_CHECKING(for BLACKHAT installation directory);
      case "${enableval}" in
        no)  AC_MSG_RESULT(BLACKHAT not enabled); blackhat=false ;;
        yes)  if test -x "$BLACKHATDIR/bin/blackhat-config"; then
                CONDITIONAL_BLACKHATDIR="$BLACKHATDIR"
                CONDITIONAL_BLACKHATINCS="-I$($CONDITIONAL_BLACKHATDIR/bin/blackhat-config --include)";
                CONDITIONAL_BLACKHATLIBS="$($CONDITIONAL_BLACKHATDIR/bin/blackhat-config --libs)"
              else
                AC_MSG_ERROR(\$BLACKHATDIR is not a valid path.);
              fi;
              AC_MSG_RESULT([${CONDITIONAL_BLACKHATDIR}]); blackhat=true;;
        *)    if test -x "${enableval}/bin/blackhat-config"; then
                CONDITIONAL_BLACKHATDIR="${enableval}"
                CONDITIONAL_BLACKHATINCS="-I$($CONDITIONAL_BLACKHATDIR/bin/blackhat-config --include)";
                CONDITIONAL_BLACKHATLIBS="$($CONDITIONAL_BLACKHATDIR/bin/blackhat-config --libs)"
              else
                AC_MSG_ERROR(${enableval} is not a valid path.);
              fi;
              AC_MSG_RESULT([${CONDITIONAL_BLACKHATDIR}]); blackhat=true;;
      esac
      ],
    [ blackhat=false ]
  )
  if test "$blackhat" = "true" ; then
    AC_DEFINE_UNQUOTED([BLACKHAT_PATH], "$CONDITIONAL_BLACKHATDIR", [BlackHat directory])
    AC_DEFINE([USING__BLACKHAT], "1", [Using BLACKHAT])
  fi
  AC_SUBST(CONDITIONAL_BLACKHATDIR)
  AC_SUBST(CONDITIONAL_BLACKHATINCS)
  AC_SUBST(CONDITIONAL_BLACKHATLIBS)
  AM_CONDITIONAL(BLACKHAT_SUPPORT, test "$blackhat" = "true")

  AC_ARG_ENABLE(
    openloops,
    AC_HELP_STRING([--enable-openloops=/path/to/openloops], [Enable OpenLoops.]),
    [ AC_MSG_CHECKING(for OpenLoops installation directory);
      case "${enableval}" in
        no)  AC_MSG_RESULT(OpenLoops not enabled); openloops=false ;;
        *)   OPENLOOPS_PREFIX="$(echo ${enableval} | sed -e 's/\/$//g')"
             openloops=true;
             if test -d "${OPENLOOPS_PREFIX}"; then
                AC_MSG_RESULT([${OPENLOOPS_PREFIX}]);
             else
                AC_MSG_WARN(${OPENLOOPS_PREFIX} is not a valid path.);
             fi;;
      esac
      ],
    [ openloops=false ]
  )
  if test "$openloops" = "true" ; then
    AC_DEFINE_UNQUOTED([OPENLOOPS_PREFIX], "$OPENLOOPS_PREFIX", [Openloops installation prefix])
  fi
  AM_CONDITIONAL(OPENLOOPS_SUPPORT, test "$openloops" = "true")

  AC_ARG_ENABLE(
    gosam,
    AC_HELP_STRING([--enable-gosam=/path/to/gosam], [Enable GoSam.]),
    [ AC_MSG_CHECKING(for GoSam installation directory);
      case "${enableval}" in
        no)  AC_MSG_RESULT(GoSam not enabled); gosam=false ;;
        *)   GOSAM_PREFIX="$(echo ${enableval} | sed -e 's/\/$//g')"
             gosam=true;
             if test -d "${GOSAM_PREFIX}"; then
                AC_MSG_RESULT([${GOSAM_PREFIX}]);
             else
                AC_MSG_WARN(${GOSAM_PREFIX} is not a valid path.);
             fi;;
      esac
      ],
    [ gosam=false ]
  )
  if test "$gosam" = "true" ; then
    AC_DEFINE_UNQUOTED([GOSAM_PREFIX], "$GOSAM_PREFIX", [GoSam installation prefix])
  fi
  AM_CONDITIONAL(GOSAM_SUPPORT, test "$gosam" = "true")

  AC_ARG_ENABLE(
    mcfm,
    AC_HELP_STRING([--enable-mcfm=/path/to/mcfm], [Enable MCFM.]),
    [ AC_MSG_CHECKING(for MCFM installation directory);
      case "${enableval}" in
        no)  AC_MSG_RESULT(MCFM not enabled); mcfm=false ;;
        yes)  if test -d "$MCFMDIR"; then
                CONDITIONAL_MCFMDIR="$MCFMDIR"
                CONDITIONAL_MCFMLIBS="-Wl,-R -Wl,$CONDITIONAL_MCFMDIR/lib -L$CONDITIONAL_MCFMDIR/lib -lMCFM"
                CONDITIONAL_MCFMINCS="-I$CONDITIONAL_MCFMDIR/include"
              else
                AC_MSG_ERROR(\$MCFMDIR is not a valid path.);
              fi;
              AC_MSG_RESULT([${CONDITIONAL_MCFMDIR}]); mcfm=true;;
        *)    if test -d "${enableval}"; then
                CONDITIONAL_MCFMDIR="${enableval}"
                CONDITIONAL_MCFMLIBS="-Wl,-R -Wl,$CONDITIONAL_MCFMDIR/lib -L$CONDITIONAL_MCFMDIR/lib -lMCFM"
                CONDITIONAL_MCFMINCS="-I$CONDITIONAL_MCFMDIR/include"
              else
                AC_MSG_ERROR(${enableval} is not a valid path.);
              fi;
              AC_MSG_RESULT([${CONDITIONAL_MCFMDIR}]); mcfm=true;;
      esac
      ],
    [ mcfm=false ]
  )
  if test "$mcfm" = "true" ; then
    AC_DEFINE([USING__MCFM], "1", [Using MCFM])
    AC_DEFINE_UNQUOTED([MCFM_PATH], "$CONDITIONAL_MCFMDIR", [MCFM directory])
  fi
  AC_SUBST(CONDITIONAL_MCFMDIR)
  AC_SUBST(CONDITIONAL_MCFMLIBS)
  AC_SUBST(CONDITIONAL_MCFMINCS)
  AM_CONDITIONAL(MCFM_SUPPORT, test "$mcfm" = "true")

  AC_ARG_ENABLE(
    lhole,
    AC_HELP_STRING([--enable-lhole], [Enable Les Houches One-Loop Generator interface.]),
    [ AC_MSG_CHECKING(for LHOLE)
      case "${enableval}" in
        no)  AC_MSG_RESULT(no); lhole=false ;;
        yes) AC_MSG_RESULT(yes); lhole=true ;;
      esac ],
    [ AC_MSG_CHECKING(for LHOLE); AC_MSG_RESULT(no); lhole=false ]
  )
  AM_CONDITIONAL(USING__LHOLE, test "$lhole" = "true" )

  lhapdfversion=5
  AC_ARG_ENABLE(
    lhapdf,
    AC_HELP_STRING([--enable-lhapdf=/path/to/lhapdf], [Enable LHAPDF support and specify where it is installed.]),
    [ AC_MSG_CHECKING(for LHAPDF installation directory);
      case "${enableval}" in
        no)  AC_MSG_RESULT(LHAPDF not enabled); lhapdf=false ;;
        yes) if test -x "`which lhapdf-config`"; then
               CONDITIONAL_LHAPDFDIR=`lhapdf-config --prefix`;
             fi;;
        *)  if test -d "${enableval}"; then
              CONDITIONAL_LHAPDFDIR=${enableval};
            fi;;
      esac;

      if test -x "$CONDITIONAL_LHAPDFDIR/bin/lhapdf-config"; then
        CONDITIONAL_LHAPDFLIBS="$($CONDITIONAL_LHAPDFDIR/bin/lhapdf-config --ldflags)";
        CONDITIONAL_LHAPDFINCS="$($CONDITIONAL_LHAPDFDIR/bin/lhapdf-config --cppflags)";
        lhapdfversion="$($CONDITIONAL_LHAPDFDIR/bin/lhapdf-config --version)";
        lhapdfversion=${lhapdfversion:0:1}
        AC_MSG_RESULT([${CONDITIONAL_LHAPDFDIR}]); lhapdf=true;
      else
        AC_MSG_ERROR(Unable to use LHAPDF from specified path.);
      fi;
    ],
    [ lhapdf=false ]
  )
  if test "$lhapdf" = "true" ; then
    AC_DEFINE_UNQUOTED([LHAPDF_PATH], "$CONDITIONAL_LHAPDFDIR", [LHAPDF directory])
    AC_DEFINE([USING__LHAPDF], "1", [using LHAPDF])
    if test [ "$lhapdfversion" -ge "6" ] ; then
      AC_DEFINE(USING__LHAPDF6, "1", [using LHAPDF6])
    fi
  fi
  AC_SUBST(CONDITIONAL_LHAPDFDIR)
  AC_SUBST(CONDITIONAL_LHAPDFLIBS)
  AC_SUBST(CONDITIONAL_LHAPDFINCS)
  AM_CONDITIONAL(LHAPDF_SUPPORT, test "$lhapdf" = "true")
  AM_CONDITIONAL(LHAPDF6_SUPPORT, test [ "$lhapdfversion" -ge "6" ])

  AC_ARG_ENABLE(
    libhdf5,
    AC_HELP_STRING([--enable-libhdf5], [Enable libhdf5 support]),
    [ case "${enableval}" in
        no)   AC_MSG_RESULT(hdf5 not enabled); libhdf5=false ;;
        yes)  AC_CHECK_LIB(hdf5, H5close, [libhdf5_found=yes], [libhdf5_found=no])
              if test "$libhdf5_found" = "yes"; then
	      	libhdf5=true;
                CONDITIONAL_HDF5LIBS="-lhdf5";
              else
                AC_MSG_ERROR(Library libhdf5 or header H5File.hpp not found.);
              fi;;
	*) if test -d "${enableval}"; then
             HDF5_OLD_LDFLAGS=$LDFLAGS;
             LDFLAGS="$LDFLAGS -L${enableval}"
             AC_CHECK_LIB(hdf5, H5close,hdf5_cv_libhdf5=yes,hdf5_cv_libhdf5=no)
             if test "$hdf5_cv_libhdf5" = "yes"; then
               libhdf5=true;
               CONDITIONAL_HDF5LIBS="-Wl,-rpath -Wl,${enableval} -L${enableval} -lhdf5"
	       AC_MSG_RESULT(Using libhdf5 from ${enableval})
             else
               AC_MSG_ERROR(Library libhdf5 not found.);
             fi
             LDFLAGS="$HDF5_OLD_LDFLAGS";
	   else
             AC_MSG_ERROR(no such directory '${enableval}');
           fi;;
      esac ],
    [ hdf5=false ]
  )
  if test "$libhdf5" = "true" ; then
    AC_DEFINE([USING__HDF5], "1", [using libhdf5])
  fi
  AM_CONDITIONAL(HDF5_SUPPORT, test "$libhdf5" = "true")
  AC_SUBST(CONDITIONAL_HDF5LIBS)

  AC_ARG_ENABLE(
    hdf5include,
    AC_HELP_STRING([--enable-hdf5include], [Enable hdf5 header support]),
    [ case "${enableval}" in
        no)   AC_MSG_RESULT(hdf5 not enabled); hdf5include=false ;;
        yes)  OLD_CXX=$CXX; CXX="$MPICXX";
	      AC_CHECK_HEADER(H5Ipublic.h, [hdf5h_found=yes], [hdf5h_found=no])
	      CXX=$OLD_CXX;
              if test test "$hdf5h_found" = "yes"; then
                hdf5include=true;
              else
                AC_MSG_ERROR(Header H5Ipublic.h not found.);
              fi;;
	*) if test -d "${enableval}"; then
             HDF5_OLD_CPPFLAGS=$CPPFLAGS; OLD_CC=$CC;
             CPPFLAGS="$CPPFLAGS -I${enableval}"; CC="$CXX";
             AC_CHECK_HEADER(H5Ipublic.h,hdf5_cv_hdf5_h=yes,hdf5_cv_hdf5_h=no)
             if test "$hdf5_cv_hdf5_h" = "yes"
             then
               hdf5include=true;
               CONDITIONAL_HDF5INCS="-I${enableval}"
	       AC_MSG_RESULT(Using hdf5 headers from ${enableval})
             else
               AC_MSG_ERROR(Header H5Ipublic.h not found.);
             fi
             CPPFLAGS="$HDF5_OLD_CPPFLAGS"; CC=$OLD_CC;
	   else
             AC_MSG_ERROR(no such directory '${enableval}');
           fi;;
      esac ],
    [ hdf5include=false ]
  )
  AC_SUBST(CONDITIONAL_HDF5INCS)

  AC_ARG_ENABLE(
    binreloc,
    AC_HELP_STRING([--enable-binreloc], [Enable binrelocing]),
    [ AC_MSG_CHECKING(whether to install relocatable Sherpa)
      case "${enableval}" in
        no)  AC_MSG_RESULT(no); binreloc=false ;;
        yes) AC_MSG_RESULT(yes); binreloc=true ;;
      esac ],
    [ AC_MSG_CHECKING(whether to install relocatable Sherpa); AC_MSG_RESULT(no); binreloc=false ] 
  )
  if test "$binreloc" = "true" ; then
    AC_DEFINE([ENABLE_BINRELOC], "1", [binreloc activation])
  fi

  AC_ARG_ENABLE(pyext,
    AC_HELP_STRING([--enable-pyext], [Enable Python API]),
    [ AC_MSG_CHECKING(for Python extension)
      case "${enableval}" in
        no) AC_MSG_RESULT(no); pyext=false ;;
        yes)  AC_MSG_RESULT(yes); pyext=true ;;
      esac ],
    [ AC_MSG_CHECKING(for Python extension); AC_MSG_RESULT(no); pyext=false])
  if test x$pyext == xtrue; then
    AM_PATH_PYTHON
    AX_PYTHON_DEVEL
    dnl Note that we need swig 2.0.12 or later for C++11 compatibility
    AX_PKG_SWIG([2.0.12],[],[ AC_MSG_ERROR([SWIG is required to build..]) ])
    AX_SWIG_ENABLE_CXX
    AX_SWIG_MULTI_MODULE_SUPPORT
    AX_SWIG_PYTHON
  fi
  AM_CONDITIONAL(ENABLE_PYEXT, [test x$pyext == xtrue])

  AC_ARG_WITH([libzip],
    AS_HELP_STRING(
      [--with-libzip=@<:@ARG@:>@],
      [use libzip library from @<:@ARG@:>@]
    ),
    [
      if test "$withval" = "install"; then
      ac_libzip_path=$ac_default_prefix;
      test "x$prefix" != xNONE && ac_libzip_path=$prefix;
      if ! test -f ${ac_libzip_path}/include/zip.h; then
        wget https://libzip.org/download/libzip-1.2.0.tar.gz
        tar xzf libzip-1.2.0.tar.gz;
        cd libzip-1.2.0;
        ./configure --prefix=${ac_libzip_path} || exit;
        make || exit; make install || exit;
        mv ${ac_libzip_path}/lib/libzip/include/zipconf.h ${ac_libzip_path}/include/;
        cd ..;
        rm -rf libzip-1.2.0.tar.gz libzip-1.2.0;
        echo "Successfully installed libzip into ${ac_libzip_path}."
      fi
      else
        ac_libzip_path="$withval"
      fi
    ],
    [ ac_libzip_path=/usr; ]
  )
  if ! test -f ${ac_libzip_path}/include/zip.h; then
    AC_MSG_ERROR(Did not find required dependency libzip in ${ac_libzip_path}. Please specify its installation prefix using '--with-libzip=/path' or enable its automatic installation using '--with-libzip=install'.)
  fi
  LIBZIP_CPPFLAGS="-I$ac_libzip_path/include -I$ac_libzip_path/lib/libzip/include"
  LIBZIP_LDFLAGS="-L$ac_libzip_path/lib -L$ac_libzip_path/lib64 -lzip"
  AC_SUBST(LIBZIP_CPPFLAGS)
  AC_SUBST(LIBZIP_LDFLAGS)

])
